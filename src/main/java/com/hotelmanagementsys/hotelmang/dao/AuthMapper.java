package com.hotelmanagementsys.hotelmang.dao;

import com.hotelmanagementsys.hotelmang.entity.User;

public interface AuthMapper {
	User register(User userToAdd);


	@Select("select * from user WHERE name=#{username} and password = #{password} ")
	@Results(value = {
		@Result(column = "id", property = "id", id = true),
		@Result(column = "chiName", property = "chiName"),
		@Result(column = "engName", property = "engName"),
		@Result(column = "telephone", property = "telephone")
})
	String login(String username, String password);

	String refresh(String oldToken);
}
