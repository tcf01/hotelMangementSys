package com.hotelmanagementsys.hotelmang.dao;

import com.hotelmanagementsys.hotelmang.controller.authController.RegisterInfoFromFN;
import com.hotelmanagementsys.hotelmang.entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface UserMapper {
	List<User> getAll();

	User getUserById(@Param("id") Integer id);

	User findByUsername(@Param("username") String username);

	@Insert("INSERT INTO users(id, username, password, sex, chiName, engName,telephone,email) " +
			"VALUES(null, #{username}, #{password}, #{sex}, #{chiName}, #{engName}, #{telephone}, #{email})")
	@Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
	Integer insertUser(User user);

	//	===================testing are as below=============================
	int deleteByPrimaryKey(String id);

	int insert(User record);

	int insertSelective(User record);

	User selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(User record);

	int updateByPrimaryKey(User record);
}
