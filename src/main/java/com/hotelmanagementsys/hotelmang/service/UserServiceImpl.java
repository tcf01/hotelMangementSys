package com.hotelmanagementsys.hotelmang.service;

import com.hotelmanagementsys.hotelmang.dao.AuthMapper;
import com.hotelmanagementsys.hotelmang.dao.UserMapper;
import com.hotelmanagementsys.hotelmang.entity.User;
import com.hotelmanagementsys.hotelmang.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class UserServiceImpl {
	@Autowired
	private UserMapper userMapper;

	@Autowired
	private AuthMapper authMapper;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;


	public List<User> getAll() {
		return userMapper.getAll();
	}

	public User getUserById(Integer id) {
		System.out.println("id passed in: " + id);
		return userMapper.getUserById(id);
	}

	public User findByUsername(String username) {
		return userMapper.findByUsername(username);
	}

	public Boolean checkPassword(String password, String passwordFromBackend) {
		return bCryptPasswordEncoder.matches(passwordFromBackend, password);
	}

	public Integer registerUser(User user) {
		//如果USERNAME撞左，應該係即時有提醒，而唔係禁完SUBMIT之後先講，呢個諗計解決 -> add a 'check' button in fn
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		//未INSERT就GET果個USER既ID就只會係NULL
		System.out.println("fajsl" + user.getId());
		userMapper.insertUser(user);

		//呢度因為INSERT完USER, 所以會拎到果個USER既ID
		return user.getId();
	}

	//	===================testing are as below=============================

	public User signup(User user) {
		try {
			user.setId(UUID.randomUUID().toString());
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			userMapper.insertSelective(user);
			return user;

		} catch (Exception e) {
			System.out.print(e);
			return user;
		}
	}

	public User login(User user){
		User userDB = authMapper.login(user.getUsername(), user.getPassword());

		if(userDB != null){
			return userDB;
		}

		throw new RuntimeException("login failed");
	} 

}
